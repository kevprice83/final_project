require 'rails_helper'
require 'spec_helper'

RSpec.describe UsersController, type: :controller do
  before :each do
    include Devise::TestHelpers

    @user = create(:kevin)
    sign_in @user
  end


  describe "GET #show" do
    it "renders the 'show' template" do
      get :show
      expect(response).to render_template('show')
    end
  end
end