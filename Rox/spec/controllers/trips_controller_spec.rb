require 'rails_helper'
require 'spec_helper'


RSpec.describe TripsController, type: :controller do
  before do
    include Devise::TestHelpers

    Trip.destroy_all
    @trip = create(:trip)

    @user = create(:kevin)
    sign_in @user   
  end

  describe "GET #index" do
    it "populates a list of trips" do
      get :index
      expect(assigns(:trips).count).to eq(1)
    end

    it "it populates a list of trips with correct attributes" do
      get :index
      expect(assigns(:trips).first.title).to eq([@trip].first.title)
    end

    it "populates a list of trips with one trip" do
      get :index
      expect(assigns(:trips).count).to eq(1)
    end

    it "renders the index view" do
      get :index
      expect(response).to render_template :index
    end
  end

  describe "GET #show" do
    it "assigns the requested trip to @trip" do
      get :show, {:id => @trip.to_param}
      expect(assigns(:trip)).to eq(@trip)
    end

    it "renders the show view" do
      get :show, {:id => @trip.to_param}
      expect(response).to render_template :show
    end
  end

  describe "GET #new" do
    it "renders the form_for trips on the new view" do
      get :new
      expect(response).to render_template :new
    end
  end


  describe "POST #create" do
    context "with valid attributes" do
      it "saves the new trip to the database" do
        expect{ post :create, trip: FactoryGirl.attributes_for(:trip) 
          }.to change(Trip,:count).by(1)
      end

      it "redirects to the new trip path" do
      post :create, trip: FactoryGirl.attributes_for(:trip)
        expect(response).to redirect_to trips_path
      end
    end
  end

    context "with invalid attributes" do
      it "does not save the new trip to the database" do
        build(:invalid_trip)
        expect{ post :create, trip: FactoryGirl.attributes_for(:invalid_trip)
          }.not_to change(Trip,:count)
        end

      it "re-renders the new view" do
        post :create, trip: FactoryGirl.attributes_for(:trip)
        expect(response).to render_template :new
      end
    end
end


