class TripRegistrationsController < ApplicationController

  def create
    @trip_registration = current_user.trip_registrations.new(status: "pending", trip_id: params[:trip_id])
    if @trip_registration.save
        flash[:success] = "Great! Your request has been sent."
        redirect_to trip_path(id: params[:trip_id])
    else
      flash[:danger] = "we were unable to register your request."
      redirect_to root_path
    end
  end

  def update
    @trip_registration = TripRegistration.find(params[:id])

    if @trip_registration.update(status: params[:status])
      flash[:success] = "Status updated successfully!"
      render json: @trip_registration
    else
      flash[:danger] = "We couldn't update the status of this request unfortunately. Please try again."
      redirect_to trips_path
    end
  end

end
