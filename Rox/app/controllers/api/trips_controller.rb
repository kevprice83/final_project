class Api::TripsController < ApplicationController

  def index
    @trips = Trip.where.not(latitude: [nil], departure: [nil]) 
    render status: 200, json: @trips
  end
end
