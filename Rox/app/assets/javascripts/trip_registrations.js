$(function () {
  var declineRegistration = function(event) {
  event.preventDefault();

  var regId = $(event.currentTarget).data("registration-id");

  var statusUpdate = "declined";

  var url = "/trip_registrations/" + regId;

  function handleSuccess(response) {
  }

  function handleError(response) { 
  }

  $.ajaxSetup({
  headers: {
    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
  }
  });

  $.ajax({
    url: url,
    type: 'put',
    data: {status: statusUpdate},
    success: handleSuccess,
    error: handleError,
    datatype: "json"
  });
}


$('.btn-decline').on('click', declineRegistration);

});

$(function () {
  var acceptRegistration = function(event) {
  event.preventDefault();

  var regId = $(event.currentTarget).data("registration-id");

  var statusUpdate = "accepted";

  var url = "/trip_registrations/" + regId;

  function handleSuccess(response) {
  }

  function handleError(response) {
  }

  $.ajaxSetup({
  headers: {
    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
  }
  });

  $.ajax({
    url: url,
    type: 'put',
    data: {status: statusUpdate},
    success: handleSuccess,
    error: handleError,
    datatype: "json"
  });
   
}


$('.btn-accept').on('click', acceptRegistration);

});
