$(function() {

  var geocoder;
  var map;
  var infowindow;
  window.createTripInitializeMap = function () {
    geocoder = new google.maps.Geocoder();

    var mapOptions = {
      zoom: 10,
      center: new google.maps.LatLng(41.3917782, 2.1772809999999936)
    };

    map = new google.maps.Map(document.getElementById('new-trip-map'), mapOptions);
    google.maps.event.addListener(map, 'click', function(event) {
      var lat = event.latLng.lat();
      var lon = event.latLng.lng();
      addMarker(lat, lon);
      $('#latitude').val(lat);
      $('#longitude').val(lon);
      codeLatLng(lat, lon);
    });

  };

  function codeLatLng(lat, lon) {
    var infowindow = new google.maps.InfoWindow();
    var latlng = new google.maps.LatLng(lat, lon);
    geocoder.geocode({'location': latlng}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        if (results[1]) {
          map.setZoom(18);
          marker = new google.maps.Marker({
            position: latlng,
            map: map
          });
          infowindow.setContent(results[1].formatted_address);
          infowindow.open(map, marker);
        } else {
          window.alert('no results found');
        }
      } else {
        window.alert('Geocoder failed due to: ' + status);
      }
    });
  }

  function addMarker(lat, lon) {
    console.log("hello")
    var position = new google.maps.LatLng(lat, lon);
    var marker = new google.maps.Marker( {
      position: position,
      animation: google.maps.Animation.DROP,
      draggable: true,
      title: "Drag me!"
    });
    marker.setMap(map);
    map.setCenter(position);
  };

  $('#submit').on("click", getAddress);

  function getAddress(event) {
    event.preventDefault();
    var address = document.getElementById("address").value;
    getCoords(address);
  };

  function getCoords (address) {
    geocoder.geocode( { 'address' : address}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        map.setCenter(results[0].geometry.location);
        var origin = results[0].geometry.location;
        var marker = new google.maps.Marker({
          map: map,
          position: origin
        });
        $('#latitude').val(results[0].geometry.location.lat());
        $('#longitude').val(results[0].geometry.location.lng());
      } else {
        alert("There was something wrong with your address");
      };
    });
  };
  
})
