class RemoveColumnFromTripsTable < ActiveRecord::Migration
  def change
    remove_column :trips, :origin
  end
end
