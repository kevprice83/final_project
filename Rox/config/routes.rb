Rails.application.routes.draw do
  devise_for :users

  root to: 'landing#index'
  
  get'/home' => 'home#index'


  resources :users

  resources :trips, only: [:index, :new, :create, :show, :destroy] do

    resources :trip_registrations, only: [:create]
  end

  resources :trip_registrations, only: [:update, :destroy]

  get 'my_trips' => 'users#user_trip_search'

  # get '/api/trips' => 'trips#serve'

  namespace :api, defaults: { format: :json } do
    resources :trips, only: [:index]
  end

end
