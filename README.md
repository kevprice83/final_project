# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Final Project @ IronHack ###

### Final Project @ IronHack ###


Final project at the Ironhack Web Development Bootcamp.

Instructions to run the app:

clone the project
```
git clone https://kevprice83@bitbucket.org/kevprice83/ironhack_project.git
```

install the ruby dependencies
```
bundle install
```

install the node dependencies
```
npm install
```

run the migrations
```
rake db:migrate
```


run the server
```
rails s
```


to run the specs:
````
rake spec
```